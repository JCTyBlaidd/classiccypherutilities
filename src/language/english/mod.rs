
/// 26 Values
pub const ENGLISH_COUNT_MONO : &[u64;26] = &include!("mono_stats.json");

/// 26*26 Values
pub const ENGLISH_COUNT_DUAL : &[u64; 676] = &include!("dual_stats.json");

/// 26*26*26 Values
pub const ENGLISH_COUNT_QUAD : &[u64; 456976] = &include!("quad_stats.json");

