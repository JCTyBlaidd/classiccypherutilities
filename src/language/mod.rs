use super::cypher::{AZChar,AZString};

mod english;

pub struct LanguageMetadata {
    pub sorted_idx : Vec<u8>,
    pub mono_score : Vec<f64>,
    pub dual_score : Vec<f64>,
    pub quad_score : Vec<f64>,
}

pub fn convert_to_score(count: &[u64]) -> Vec<f64> {
    let mut vec = Vec::with_capacity(count.len());
    let total = (count.iter().sum::<u64>() + count.len() as u64)as f64;
    for i in  0..count.len() {
        let value = (count[i] + 1) as f64;
        let probability = value / total;
        vec.push(probability.log10());
    }
    vec
}

pub fn sort_index(count: &[u64]) -> Vec<u8> {
    let mut data = count.iter().enumerate().collect::<Vec<_>>();
    data.sort_by_key(|(_,v)| *v);
    data.reverse();
    let vec: Vec<u8> = data.iter().map(|(idx,_)| *idx as u8).collect();
    vec
}

pub fn get_english() -> LanguageMetadata {
    let sorted_idx = sort_index(english::ENGLISH_COUNT_MONO);
    let mono_score = convert_to_score(english::ENGLISH_COUNT_MONO);
    let dual_score = convert_to_score(english::ENGLISH_COUNT_DUAL);
    let quad_score = convert_to_score(english::ENGLISH_COUNT_QUAD);
    LanguageMetadata {
        sorted_idx,
        mono_score,
        dual_score,
        quad_score
    }
}

impl LanguageMetadata {

    pub fn quad_score(&self, az: &AZString) -> f64 {
        assert_eq!(self.quad_score.len(),26*26*26*26);
        let mut score = 0.0;
        for window in az.windows(4) {
            let idx : usize = (window[0].as_size() * 17576)
                + (window[1].as_size() * 676)
                + (window[2].as_size() * 26)
                + window[3].as_size();
            unsafe {
                score += self.quad_score.get_unchecked(idx);
            }
        }
        score
    }

    pub fn chi_squared(&self, az: &AZString, shift: AZChar) -> f64 {
        let mut count = [0;26];
        for value in az {
            let new_value = *value + shift;
            count[new_value.as_size()] += 1;
        }
        let mut chi = 0.0;
        for (idx,num) in count.iter().enumerate() {
            let expected = self.mono_score[idx] * (az.len() as f64);
            let diff = (*num as f64) - expected;
            chi += (diff * diff) / expected;
        }
        chi
    }
}
