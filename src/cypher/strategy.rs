use super::{AZString,Cypher,CypherKey,Rng,ResultContainer,LanguageMetadata};
use std::f64::NEG_INFINITY;
use rand::distributions::{Uniform};
use rayon::prelude::*;
use std::ops::Range;

pub fn exhaustive_search<C: Cypher<Output = AZString>> (
    cypher: &C,
    input: &C::Input,
    lang: &LanguageMetadata,
    dim: u32,
    count: usize
) -> ResultContainer {
    let mut key = C::Key::new_default(dim);
    let mut best = ResultContainer::new(count);
    let mut output = AZString::new();

    if let Ok(()) = cypher.decrypt(&key,&input, &mut output) {
        let score = lang.quad_score(&output);
        best.add(score,Box::new(key.clone()),None);
    }
    while key.advance() {
        if let Ok(()) = cypher.decrypt(&key,&input, &mut output) {
            let score = lang.quad_score(&output);
            if best.will_append(score) {
                best.add(score, Box::new(key.clone()), None);
            }
        }
    }

    best.process_all_outputs::<C>(cypher,input);

    best
}

pub fn exhaustive_search_range<C: Cypher<Output = AZString>> (
    cypher: &C,
    input: &C::Input,
    lang: &LanguageMetadata,
    range: Range<u32>,
    count: usize
) -> ResultContainer {
    let mut scores = ResultContainer::new(count);
    let mut high_scores = ResultContainer::new(count);
    let middle = range.start + (((range.end - range.start) * 2) / 3);
    rayon::scope(|scope| {
        scope.spawn(|_| {
            for dim in range.start..middle {
                let new_scores = exhaustive_search(cypher,input,lang,dim,count);
                scores.join(new_scores);
            }
        });
        scope.spawn(|_| {
            for dim in middle..range.end {
                let new_scores = exhaustive_search(cypher,input,lang,dim,count);
                high_scores.join(new_scores);
            }
        });
    });

    scores.join(high_scores);
    scores
}


pub fn async_result<F: Sync + Fn(u32) -> ResultContainer>(
    range: Range<usize>,
    f: F,
    count: usize
) -> ResultContainer{
    let mut results = Vec::new();
    for _ in range.clone() {
        results.push(None);
    }
    results.par_iter_mut().enumerate().for_each(|(i,out)| {
        let dim = i + range.start;
        let res = f(dim as u32);
        *out = Some(res);
    });
    let mut target = ResultContainer::new(count);
    for val in results.into_iter() {
        if let Some(result) = val {
            target.join(result);
        }
    }
    target
}

pub fn hill_climb_with_start<C: Cypher<Output = AZString>, R: Rng>(
    cypher: &C,
    input: &C::Input,
    lang: &LanguageMetadata,
    dim: u32,
    count: usize,
    iterations: usize,
    strength: u32,
    mutate_sf: usize,
    rng: &mut R,
    start_key: C::Key
) -> ResultContainer {
    let mut last_key : C::Key = start_key;
    let mut curr_key = last_key.clone();
    let mut best = ResultContainer::new(count);
    let mut output = AZString::with_capacity(dim as usize);

    let div_factor = iterations / mutate_sf;
    for idx in 0..iterations {
        let scale = ((iterations - idx) / div_factor) as u32 + 1;
        curr_key.mutate(rng, strength * scale);
        if let Ok(()) = cypher.decrypt(&curr_key,&input,&mut output) {
            let score = lang.quad_score(&output);
            if score > best.best_score() {
                best.add(score,Box::new(curr_key.clone()),None);
                last_key.assign(&curr_key);
            }else{
                curr_key.assign(&last_key);
            }
        }else {
            curr_key.assign(&last_key);
        }
    };

    best.process_all_outputs::<C>(cypher,input);

    best
}

pub fn hill_climb<C: Cypher<Output = AZString>, R: Rng>(
    cypher: &C,
    input: &C::Input,
    lang: &LanguageMetadata,
    dim: u32,
    count: usize,
    iterations: usize,
    strength: u32,
    mutate_sf: usize,
    rng: &mut R,
) -> ResultContainer {
    let mut start_key : C::Key = C::Key::new_random::<R>(dim,rng);

    let mut new_key;
    let mut output = AZString::new();
    let mut start_score = NEG_INFINITY;

    for _i in 0..50 {
        new_key = C::Key::new_random::<R>(dim,rng);
        if let Ok(()) = cypher.decrypt(&new_key,&input,&mut output) {
            let new_score = lang.quad_score(&output);
            if new_score > start_score {
                start_score = new_score;
                start_key.assign(&new_key);
            }
        }
    }
    hill_climb_with_start(
        cypher,
        input,
        lang,
        dim,
        count,
        iterations,
        strength,
        mutate_sf,
        rng,
        start_key
    )
}


pub fn simulated_annealing<C: Cypher<Output = AZString>, R: Rng>(
    cypher: &C,
    input: &C::Input,
    lang: &LanguageMetadata,
    dim: u32,
    count: usize,
    k_max: usize,
    mutate_sf: usize,
    rng: &mut R
) -> ResultContainer {
    let mut last_key : C::Key = C::Key::new_random::<R>(dim,rng);
    let mut curr_key = last_key.clone();
    let mut last_score = NEG_INFINITY;
    let mut best = ResultContainer::new(count);
    let mut output = AZString::new();

    let distribution = Uniform::new_inclusive(0.0,1.0);

    let section_k_max = k_max / mutate_sf;
    let k_scale = 1.0 / (k_max as f64);
    for s in 0..mutate_sf {
        for k in 0..section_k_max {
            let total_k = (s * section_k_max) + k;
            let temp= (total_k as f64) * k_scale;
            curr_key.mutate(rng,(mutate_sf - s) as u32);

            if let Ok(()) = cypher.decrypt(&curr_key, &input, &mut output) {
                let score = lang.quad_score(&output);
                let probability = if score > last_score {

                    //Increase {ACCEPT ALWAYS}
                    1.0
                }else{

                    //Decrease {SOMETIMES ACCEPT}
                    let diff = (score - last_score) / temp;
                    diff.exp()
                };

                if probability >= rng.sample(&distribution) {
                    best.add(score, Box::new(curr_key.clone()), None);
                    last_key.assign(&curr_key);
                    last_score = score;
                } else {
                    curr_key.assign(&last_key);
                }
            }else {
                curr_key.assign(&last_key);
            }
        }
    }

    best.process_all_outputs::<C>(cypher,input);
    best
}