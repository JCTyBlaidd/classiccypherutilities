use super::{AZChar,AZString,CypherError,Cypher,CypherKey,Rng,Range,advance_slice};
use std::fmt::Display;
use std::fmt;

#[derive(Clone, Default, Debug)]
pub struct Hill2x2Key {
    key: [AZChar; 4]
}
#[derive(Clone,Default,Debug)]
pub struct Hill3x3Key {
    key:[AZChar; 9]
}

impl Hill2x2Key {
    fn det(&self) -> AZChar {
        const OFFSET: usize = 26 * 26;
        let a = self.key[0].as_size() * self.key[3].as_size();
        let b = self.key[1].as_size() * self.key[2].as_size();
        AZChar::from_size(OFFSET + a - b)
    }
}

impl Hill3x3Key {
    fn det(&self) -> AZChar {
        const OFFSET_1: usize = 26 * 26 * 26;
        const OFFSET_2: usize = 26 * 26 * 26 * 26;
        let key = &self.key;
        let a = OFFSET_1 + (key[4].as_size() * key[8].as_size())
            - (key[7].as_size() * key[5].as_size());
        let b = OFFSET_1 + (key[1].as_size() * key[8].as_size())
            - (key[7].as_size() * key[2].as_size());
        let c = OFFSET_1 + (key[1].as_size() * key[5].as_size())
            - (key[4].as_size() * key[2].as_size());
        AZChar::from_size(OFFSET_2 + (key[0].as_size() * a)
            + (key[6].as_size() * c) - (key[3].as_size() * b))
    }
}

impl Display for Hill2x2Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "Hill( [{:?},{:?}] )",
            &self.key[..2].iter().map(|c| c.as_size()),
            &self.key[2..].iter().map(|c| c.as_size())
        )
    }
}
impl Display for Hill3x3Key {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(
            f,
            "Hill( [{:?},{:?},{:?}] )",
            &self.key[..3].iter().map(|c| c.as_size()),
            &self.key[3..6].iter().map(|c| c.as_size()),
            &self.key[6..].iter().map(|c| c.as_size())
        )
    }
}

impl CypherKey for Hill2x2Key {
    fn dimensionality() -> Range<u32> {
        2..3
    }
    fn new_default(_size: u32) -> Self {
        Hill2x2Key {
            key: Default::default(),
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        let mut val = Self::new_default(size);
        for value in val.key.iter_mut() {
            *value = AZChar::new(rng.gen_range(0,26));
        }
        val
    }
    fn mutate<R: Rng>(&mut self, rng: &mut R, strength: u32) {
        for _i in 0..strength {
            if let Some(cell) = rng.choose_mut(&mut self.key) {
                *cell = AZChar::new(rng.gen_range(0,26));
            }
        }
    }
    fn advance(&mut self) -> bool {
        advance_slice(&mut self.key)
    }
    fn assign(&mut self, other: &Hill2x2Key) {
        self.key.copy_from_slice(&other.key)
    }
}

impl CypherKey for Hill3x3Key {
    fn dimensionality() -> Range<u32> {
        3..4
    }
    fn new_default(_size: u32) -> Self {
        Hill3x3Key {
            key: Default::default(),
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        let mut val = Self::new_default(size);
        for value in val.key.iter_mut() {
            *value = AZChar::new(rng.gen_range(0,26));
        }
        val
    }
    fn mutate<R: Rng>(&mut self, rng: &mut R, strength: u32) {
        for _i in 0..strength {
            if let Some(cell) = rng.choose_mut(&mut self.key) {
                *cell = AZChar::new(rng.gen_range(0,26));
            }
        }
    }
    fn advance(&mut self) -> bool {
        advance_slice(&mut self.key)
    }
    fn assign(&mut self, other: &Hill3x3Key) {
        self.key.copy_from_slice(&other.key)
    }
}

pub struct Hill2x2Cypher;
pub struct Hill3x3Cypher;

impl Cypher for Hill2x2Cypher {
    type Key = Hill2x2Key;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key: &Hill2x2Key, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let matrix = &key.key;
        if input.len() % 2 == 0 {
            for (i,o) in input.chunks(2).zip(output.chunks_mut(2)) {
                unsafe {
                    debug_assert_eq!(i.len(),2);
                    debug_assert_eq!(o.len(),2);
                    let a: usize = i.get_unchecked(0).as_size();
                    let b: usize = i.get_unchecked(1).as_size();
                    *o.get_unchecked_mut(0) = AZChar::from_size(a * matrix[0].as_size() + b * matrix[1].as_size());
                    *o.get_unchecked_mut(1) = AZChar::from_size(a * matrix[2].as_size() + b * matrix[3].as_size());
                }
            }
            Ok(())
        }else{
            Err(CypherError::InputError)
        }
    }
    fn decrypt(&self, key: &Hill2x2Key, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        if let Some(inv_det) = key.det().mul_inverse() {
            let matrix : [AZChar;4] = [
                 key.key[3] * inv_det,
                -key.key[1] * inv_det,
                -key.key[2] * inv_det,
                 key.key[0] * inv_det
            ];
            if input.len() % 2 == 0 {
                for (i,o) in input.chunks(2).zip(output.chunks_mut(2)) {
                    unsafe {
                        debug_assert_eq!(i.len(),2);
                        debug_assert_eq!(o.len(),2);
                        let a: usize = i.get_unchecked(0).as_size();
                        let b: usize = i.get_unchecked(1).as_size();
                        *o.get_unchecked_mut(0) = AZChar::from_size(a * matrix[0].as_size() + b * matrix[1].as_size());
                        *o.get_unchecked_mut(1) = AZChar::from_size(a * matrix[2].as_size() + b * matrix[3].as_size());
                    }
                }
                Ok(())
            } else {
                Err(CypherError::InputError)
            }
        }else{
            Err(CypherError::KeyError)
        }
    }
}

impl Cypher for Hill3x3Cypher {
    type Key = Hill3x3Key;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key: &Hill3x3Key, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        if input.len() % 3 == 0 {
            let matrix = &key.key;
            for (i,o) in input.chunks(3).zip(output.chunks_mut(3)) {
                unsafe {
                    debug_assert_eq!(i.len(),3);
                    debug_assert_eq!(o.len(),3);
                    let a: usize = i.get_unchecked(0).as_size();
                    let b: usize = i.get_unchecked(1).as_size();
                    let c: usize = i.get_unchecked(2).as_size();
                    *o.get_unchecked_mut(0) = AZChar::from_size(
                        a * matrix[0].as_size() + b * matrix[1].as_size() + c * matrix[2].as_size()
                    );
                    *o.get_unchecked_mut(1) = AZChar::from_size(
                        a * matrix[3].as_size() + b * matrix[4].as_size() + c * matrix[5].as_size()
                    );
                    *o.get_unchecked_mut(2) = AZChar::from_size(
                        a * matrix[6].as_size() + b * matrix[7].as_size() + c * matrix[8].as_size()
                    );
                }
            }
            Ok(())
        }else{
            Err(CypherError::InputError)
        }
    }
    fn decrypt(&self, key: &Hill3x3Key, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        if let Some(inv_det) = key.det().mul_inverse() {
            let m = &key.key;
            let matrix : [AZChar;9] = [
                 (m[4] * m[8] - m[7] * m[5]) * inv_det,
                -(m[1] * m[8] - m[7] * m[2]) * inv_det,
                 (m[1] * m[5] - m[4] * m[2]) * inv_det,
                -(m[3] * m[8] - m[6] * m[5]) * inv_det,
                 (m[0] * m[8] - m[6] * m[2]) * inv_det,
                -(m[0] * m[5] - m[3] * m[2]) * inv_det,
                 (m[3] * m[7] - m[6] * m[4]) * inv_det,
                -(m[0] * m[7] - m[6] * m[1]) * inv_det,
                 (m[0] * m[4] - m[3] * m[1]) * inv_det
            ];

            if input.len() % 2 == 0 {
                for (i,o) in input.chunks(3).zip(output.chunks_mut(3)) {
                    unsafe {
                        debug_assert_eq!(i.len(),3);
                        debug_assert_eq!(o.len(),3);
                        let a: usize = i.get_unchecked(0).as_size();
                        let b: usize = i.get_unchecked(1).as_size();
                        let c: usize = i.get_unchecked(2).as_size();
                        *o.get_unchecked_mut(0) = AZChar::from_size(
                            a * matrix[0].as_size() + b * matrix[1].as_size() + c * matrix[2].as_size()
                        );
                        *o.get_unchecked_mut(1) = AZChar::from_size(
                            a * matrix[3].as_size() + b * matrix[4].as_size() + c * matrix[5].as_size()
                        );
                        *o.get_unchecked_mut(2) = AZChar::from_size(
                            a * matrix[6].as_size() + b * matrix[7].as_size() + c * matrix[8].as_size()
                        );
                    }
                }
                Ok(())
            } else {
                Err(CypherError::InputError)
            }
        }else{
            Err(CypherError::KeyError)
        }
    }
}


#[cfg(test)]
mod test {
    use super::{AZChar,Hill2x2Cypher,Hill2x2Key,Hill3x3Cypher,Hill3x3Key};
    use super::super::test;

    #[test]
    fn test_encrypt() {
        let cypher : Hill2x2Cypher = Hill2x2Cypher;
        let mut key = Hill2x2Key {
            key: [AZChar::new(1),Default::default(),Default::default(),AZChar::new(1)]
        };
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        );
        key.key = [AZChar::new(5),AZChar::new(17),AZChar::new(4),AZChar::new(15)];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "RPJBBNTZLLDXVJNVFHXTPFHRZD"
        );
        key.key = [AZChar::new(8),AZChar::new(25),AZChar::new( 9),AZChar::new( 2)];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ZCNYBUPQDMRIFETAHWVSJOXKLG"
        );
        key.key = [Default::default(),AZChar::new(1),AZChar::new(1),Default::default()];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "BADCFEHGJILKNMPORQTSVUXWZY"
        );

        let alt_cypher : Hill3x3Cypher = Hill3x3Cypher;
        let mut alt_key = Hill3x3Key {
            key: [
                AZChar::new(1),Default::default(),Default::default(),
                Default::default(),AZChar::new(1),Default::default(),
                Default::default(),Default::default(),AZChar::new(1)
            ]
        };
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWX",
            "ABCDEFGHIJKLMNOPQRSTUVWX"
        );
        alt_key.key = [
            AZChar::new(1),AZChar::new(16),AZChar::new(13),
            AZChar::new(25),AZChar::new(9),AZChar::new(2),
            AZChar::new(7),AZChar::new(5),AZChar::new(21)
        ];
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWX",
            "QNVCRQOVLAZGMDBYHWKLRWPM"
        );
        alt_key.key = [
            AZChar::new(23),AZChar::new(14),AZChar::new(8),
            AZChar::new(1),AZChar::new(9),AZChar::new(7),
            AZChar::new(19),AZChar::new(6),AZChar::new(17)
        ];
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWX",
            "EXOJWKOVGTUCYTYDSUIRQNQM"
        );
        alt_key.key = [
            AZChar::new(23),AZChar::new(4),AZChar::new(18),
            AZChar::new(12),AZChar::new(9),AZChar::new(12),
            AZChar::new(19),AZChar::new(22),AZChar::new(15)
        ];
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWX",
            "OHATCMYXYDSKINWNIISDUXYG"
        );
    }
}