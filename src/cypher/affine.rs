use super::{AZChar,AZString,CypherError,Cypher,CypherKey,Rng,Range};
use std::fmt::Display;
use std::fmt;

#[derive(Copy, Clone, Default, Debug)]
pub struct AffineKey {
    shift: u8,
    offset: u8
}

impl Display for AffineKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        if self.shift == 1 {
            write!(f, "Caeser( {} )",self.offset)
        }else {
            write!(f, "Affine( {} * x + {} )", self.shift, self.offset)
        }
    }
}

static VALID_SHIFT : [u8; 12] = [1,3,5,7,9,11,15,17,19,21,23,25];


impl CypherKey for AffineKey {
    fn dimensionality() -> Range<u32> {
        2..3
    }
    fn new_default(_size: u32) -> Self {
        AffineKey {
            shift : 1,
            offset: 0,
        }
    }
    fn new_random<R: Rng>(_size: u32, rng: &mut R) -> Self {
        AffineKey {
            shift: *rng.choose(&VALID_SHIFT).unwrap(),
            offset: rng.gen_range(0,26),
        }
    }
    fn mutate<R: Rng>(&mut self,rng: &mut R, _strength: u32) {
        if rng.gen_bool(0.5) {
            self.shift = *rng.choose(&VALID_SHIFT).unwrap();
        } else {
            self.offset = rng.gen_range(0,26);
        }
    }
    fn advance(&mut self) -> bool {
        if self.shift == 25 && self.offset == 25 {
            false
        }else {
            self.offset += 1;
            if self.offset > 25 {
                self.offset = 0;
                self.shift += 2;
                if self.shift == 13 {
                    self.shift = 15
                }
            }
            true
        }
    }
    fn assign(&mut self, other: &AffineKey) {
        self.shift = other.shift;
        self.offset = other.offset;
    }
}


pub struct AffineCypher;

impl Cypher for AffineCypher {
    type Key = AffineKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key : &AffineKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mul_v = AZChar::new(key.shift);
        let add_v = AZChar::new(key.offset);
        for (i,o) in input.iter().zip(output.iter_mut()) {
            *o = (*i * mul_v) + add_v;
        }
        Ok(())
    }
    fn decrypt(&self, key : &AffineKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mul_v = AZChar::new(key.shift);
        let add_v = AZChar::new(key.offset);
        match mul_v.mul_inverse() {
            Some(inv_v) => {
                for (i,o) in input.iter().zip(output.iter_mut()) {
                    *o = (*i - add_v) * inv_v
                }
                Ok(())
            }
            None => {
                Err(CypherError::KeyError)
            }
        }
    }
}


#[cfg(test)]
mod test {
    use super::{AffineCypher,AffineKey};
    use super::super::test;
    #[test]
    fn test_encrypt() {
        let cypher : AffineCypher = AffineCypher;
        let key1 = AffineKey {
            shift: 1,
            offset: 6,
        };
        test::test_encrypt(
            &cypher,
            &key1,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "GHIJKLMNOPQRSTUVWXYZABCDEF"
        );
        let key2 = AffineKey {
            shift: 5,
            offset: 8,
        };
        test::test_encrypt(
            &cypher,
            &key2,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "INSXCHMRWBGLQVAFKPUZEJOTYD"
        );
    }
}