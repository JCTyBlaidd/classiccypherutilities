use super::{AZChar,AZString,CypherError,Cypher,CypherKey,Rng,Range,convert_from_az,advance_slice};
use std::fmt::Display;
use std::fmt;
use smallvec::SmallVec;

#[derive(Clone, Default, Debug)]
pub struct VignereKey {
    key : SmallVec<[AZChar;12]>,
}
#[derive(Clone, Default, Debug)]
pub struct AutoKeyKey {
    key: SmallVec<[AZChar;12]>
}

impl Display for VignereKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f,"Vignere( \"{}\" )",convert_from_az(&self.key.as_slice()))
    }
}
impl Display for AutoKeyKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f,"Autokey( \"{}\" )",convert_from_az(&self.key.as_slice()))
    }
}


impl CypherKey for VignereKey {
    fn dimensionality() -> Range<u32> {
        1..26
    }
    fn new_default(size: u32) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize,Default::default());
        VignereKey {
            key
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize, Default::default());
        for k in key.iter_mut() {
            *k = AZChar::new(rng.gen_range(0,26))
        }
        VignereKey {
            key
        }
    }
    fn mutate<R: Rng>(&mut self,rng: &mut R, strength: u32) {
        for _ in 0..strength {
            let idx = rng.gen_range(0,self.key.len());
            self.key[idx].set_u8(rng.gen_range(0,26) as u8);
        }
    }
    fn advance(&mut self) -> bool {
        advance_slice(self.key.as_mut_slice())
    }
    fn assign(&mut self,other: &VignereKey) {
        self.key.copy_from_slice(&other.key);
    }
}
impl CypherKey for AutoKeyKey {
    fn dimensionality() -> Range<u32> {
        1..26
    }
    fn new_default(size: u32) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize,Default::default());
        AutoKeyKey {
            key
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize, Default::default());
        for k in key.iter_mut() {
            *k = AZChar::new(rng.gen_range(0,26))
        }
        AutoKeyKey {
            key
        }
    }
    fn mutate<R: Rng>(&mut self,rng: &mut R, strength: u32) {
        for _ in 0..strength {
            let idx = rng.gen_range(0,self.key.len());
            self.key[idx].set_u8(rng.gen_range(0,26) as u8);
        }
    }
    fn advance(&mut self) -> bool {
        advance_slice(self.key.as_mut_slice())
    }
    fn assign(&mut self,other: &AutoKeyKey) {
        self.key.copy_from_slice(&other.key);
    }
}


pub struct VignereCypher;
pub struct AutoKeyCypher;

impl Cypher for VignereCypher {
    type Key = VignereKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key : &VignereKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mut idx = 0;
        for (i,o) in input.iter().zip(output.iter_mut()) {
            *o = *i + key.key[idx];
            idx = (idx + 1) % key.key.len();
        }
        Ok(())
    }
    fn decrypt(&self, key : &VignereKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mut idx = 0;
        for (i,o) in input.iter().zip(output.iter_mut()) {
            *o = *i - key.key[idx];
            idx = (idx + 1) % key.key.len();
        }
        Ok(())
    }
}
impl Cypher for AutoKeyCypher {
    type Key = AutoKeyKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key : &AutoKeyKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let repeat = key.key.len();
        let (input_key, input_auto) = input.split_at(repeat);
        let (output_key, output_auto) = output.split_at_mut(repeat);
        for ((i,k),o) in input_key.iter().zip(key.key.iter())
            .zip(output_key.iter_mut())
            {
            *o = *i + *k;
        }
        for ((i,p),o) in input_auto.iter().zip(input.iter())
            .zip(output_auto.iter_mut())
            {
            *o = *i + *p;
        }

        Ok(())
    }
    fn decrypt(&self, key : &AutoKeyKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let repeat = key.key.len();
        let (input_key, input_auto) = input.split_at(repeat);
        for ((i,k),o) in input_key.iter().zip(key.key.iter())
            .zip(output.iter_mut())
            {
            *o = *i - *k;
        }
        let mut chunks = output.chunks_mut(repeat);
        let mut prev_chunk = chunks.next().unwrap_or(&mut []);
        for (write_chunk,input_chunk) in chunks
            .zip(input_auto.chunks(repeat))
            {
            for ((o,k),i) in write_chunk.iter_mut().
                zip(prev_chunk.iter()).zip(input_chunk.iter())
                {
                *o = *i - *k;
            }
            prev_chunk = write_chunk;
        }
        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::{VignereCypher,VignereKey,AutoKeyCypher,AutoKeyKey,CypherKey};
    use super::super::test;
    use super::super::convert_to_az;
    #[test]
    fn test_encrypt() {
        let cypher : VignereCypher = VignereCypher;
        let mut key = VignereKey {
            key: convert_to_az(&"LEMON".to_string()).into_iter().collect()
        };
        test::test_encrypt(
            &cypher,
            &key,
            "ATTACKATDAWN",
            "LXFOPVEFRNHR"
        );
        key.key = convert_to_az(&"AA".to_string()).into_iter().collect();
        for i in 0..26 {
            assert_eq!(i,key.key[1].as_size());
            for j in 0..26 {
                assert_eq!(j,key.key[0].as_size());
                assert_eq!(key.advance(), i != 25 || j != 25);
            }
        }
        key.key = convert_to_az(&"AAAA".to_string()).into_iter().collect();
        for i in 0..26 {
            assert_eq!(i,key.key[3].as_size());
            for j in 0..26 {
                assert_eq!(j,key.key[2].as_size());
                for k in 0..26 {
                    assert_eq!(k,key.key[1].as_size());
                    for l in 0..26 {
                        assert_eq!(l,key.key[0].as_size());
                        assert_eq!(key.advance(), i != 25 || j != 25 || k != 25 || l != 25);
                    }
                }
            }
        }
        let alt_cypher : AutoKeyCypher = AutoKeyCypher;
        let mut alt_key = AutoKeyKey {
            key: convert_to_az(&"FAIL".to_string()).into_iter().collect()
        };
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "FBKOEGIKMOQSUWYACEGIKMOQSU"
        );
        alt_key.key = convert_to_az(&"AUTOKEY".to_string()).into_iter().collect();
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "AVVROJEHJLNPRTVXZBDFHJLNPR"
        );
    }
}