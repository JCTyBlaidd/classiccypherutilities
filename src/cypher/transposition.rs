use super::{AZString,OrderedKey,CypherError,Cypher,CypherKey,Rng,Range};
use std::fmt::Display;
use std::fmt;

#[derive(Clone, Default, Debug)]
pub struct TransposeKey {
    key: OrderedKey,
}
#[derive(Clone,Default,Debug)]
pub struct ColumnarKey {
    key: OrderedKey,
}

impl Display for TransposeKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f,"Transposition( ")?;
        for v in self.key.order.iter() {
            write!(f,"{} ",v)?;
        }
        write!(f,")")
    }
}
impl Display for ColumnarKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f,"Columnar( ")?;
        for v in self.key.order.iter() {
            write!(f,"{} ",v)?;
        }
        write!(f,")")
    }
}

impl CypherKey for TransposeKey {
    fn dimensionality() -> Range<u32> {
        1..26
    }
    fn new_default(size: u32) -> Self {
        TransposeKey {
            key: OrderedKey::from_size(size as usize),
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        TransposeKey {
            key: OrderedKey::from_rng(size as usize,rng),
        }
    }
    fn mutate<R: Rng>(&mut self, rng: &mut R, strength: u32) {
        self.key.mutate(rng,strength);
    }
    fn advance(&mut self) -> bool {
        self.key.advance()
    }
    fn assign(&mut self, other: &TransposeKey) {
        self.key.assign(&other.key);
    }
}
impl CypherKey for ColumnarKey {
    fn dimensionality() -> Range<u32> {
        1..26
    }
    fn new_default(size: u32) -> Self {
        ColumnarKey {
            key: OrderedKey::from_size(size as usize),
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        ColumnarKey {
            key: OrderedKey::from_rng(size as usize, rng),
        }
    }
    fn mutate<R: Rng>(&mut self, rng: &mut R, strength: u32) {
        self.key.mutate(rng,strength);
    }
    fn advance(&mut self) -> bool {
        self.key.advance()
    }
    fn assign(&mut self, other: &ColumnarKey) {
        self.key.assign(&other.key);
    }
}

pub struct TransposeCypher;
pub struct ColumnarCypher;

impl Cypher for TransposeCypher {
    type Key = TransposeKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key: &TransposeKey, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        let order = &key.key.order;
        let chunk_size = order.len();
        output.resize(input.len(),Default::default());
        for (i,o) in input.chunks(chunk_size).zip(output.chunks_mut(chunk_size)) {
            let size = i.len();
            if size == chunk_size {
                for idx in 0..chunk_size {
                    o[idx] = i[order[idx]]
                }
            }else {
                let mut out_idx = 0;
                for idx in 0..chunk_size {
                    let in_idx = order[idx];
                    if in_idx < size {
                        o[out_idx] = i[in_idx];
                        out_idx += 1;
                    }
                }
            }
        }
        Ok(())
    }
    fn decrypt(&self, key: &TransposeKey, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        let order = &key.key.order;
        let chunk_size = order.len();
        output.resize(input.len(),Default::default());
        for (i,o) in input.chunks(chunk_size).zip(output.chunks_mut(chunk_size)) {
            let size = i.len();
            if size == chunk_size {
                for idx in 0..chunk_size {
                    o[order[idx]] = i[idx]
                }
            }else {
                let mut out_idx = 0;
                for idx in 0..chunk_size {
                    let in_idx = order[idx];
                    if in_idx < size {
                        o[in_idx] = i[out_idx];
                        out_idx += 1;
                    }
                }
            }
        }
        Ok(())
    }
}
impl Cypher for ColumnarCypher {
    type Key = ColumnarKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key: &ColumnarKey, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let key_len = key.key.order.len();
        let block_count = input.len() / key_len;
        let block_remain = input.len() % key_len;
        if block_remain == 0 {
            for (x_out,x_in) in key.key.order.iter().enumerate() {
                for y_idx in 0..block_count {
                    let out_idx = x_out * block_count + y_idx;
                    let in_idx = y_idx * key_len + x_in;
                    unsafe {
                        debug_assert!(out_idx < output.len());
                        debug_assert!(in_idx < input.len());
                        *output.get_unchecked_mut(out_idx) = *input.get_unchecked(in_idx);
                    }
                }
            }
            Ok(())
        }else{
            Err(CypherError::InputError)
        }
    }
    fn decrypt(&self, key: &ColumnarKey, input: &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let key_len = key.key.order.len();
        let block_count = input.len() / key_len;
        let block_remain = input.len() % key_len;
        if block_remain == 0 {
            for y_idx in 0..block_count {
                for (x_out, x_in) in key.key.order.iter().enumerate() {
                    let out_idx = y_idx * key_len + x_out;
                    let in_idx = x_in * block_count + y_idx;
                    unsafe {
                        debug_assert!(out_idx < output.len());
                        debug_assert!(in_idx < input.len());
                        *output.get_unchecked_mut(out_idx) = *input.get_unchecked(in_idx);
                    }
                }
            }
            Ok(())
        }else{
            Err(CypherError::InputError)
        }
    }
}

#[cfg(test)]
mod test {
    use super::{TransposeCypher,TransposeKey,ColumnarCypher,ColumnarKey};
    use super::super::test;
    use super::super::OrderedKey;

    #[test]
    fn test_encrypt() {
        let cypher : TransposeCypher = TransposeCypher;
        let mut key = TransposeKey {
            key: OrderedKey {
                order: smallvec![1, 3, 2, 0]
            },
        };
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "BDCAFHGEJLKINPOMRTSQVXWUZY"
        );
        key.key.order = smallvec![0,1,2,3,4];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        );
        key.key.order = smallvec![4,2,3,0,1];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "ECDABJHIFGOMNKLTRSPQYWXUVZ"
        );
        key.key.order = smallvec![5,6,2,4,3,0,1];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "FGCEDABMNJLKHITUQSROPXZYVW"
        );
        key.key.order = smallvec![3,7,2,0,8,5,6,4,1];
        test::test_encrypt(
            &cypher,
            &key,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "DHCAIFGEBMQLJROPNKVZUSXYWT"
        );
        let alt_cypher : ColumnarCypher = ColumnarCypher;
        let mut alt_key = ColumnarKey {
            key: OrderedKey {
                order: smallvec![1,0,2]
            },
        };
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKL",
            "BEHKADGJCFIL"
        );
        alt_key.key.order = smallvec![0,1,2,3];
        test::test_encrypt(
            &alt_cypher,
            &alt_key,
            "ABCDEFGHIJKLMNOPQRSTUVWX",
            "AEIMQUBFJNRVCGKOSWDHLPTX"
        )
    }
}