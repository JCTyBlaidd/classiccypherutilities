use rand::Rng;
use std::ops::Range;
use std::fmt::Display;
use std::any::Any;
use super::LanguageMetadata;

//Cypher Functions
mod affine;
mod vignere;
mod keyword;
mod transposition;
mod hill;
mod amsco;

//Utility Functions
mod strategy;
mod util;
mod dynamic;

pub use self::util::{AZChar,OrderedKey,ResultContainer};
pub type AZString = Vec<AZChar>;

pub fn convert_to_az(input: &str) -> AZString {
    let mut out = AZString::new();
    for val in input.chars() {
        let upper = val.to_ascii_uppercase();
        if upper >= 'A' && upper <= 'Z' {
            out.push(AZChar::new(((upper as u32) - ('A' as u32)) as u8))
        }
    }
    out
}

pub fn convert_from_az(input: &[AZChar]) -> String {
    let mut out = String::new();
    for val in input {
        let char_val = val.as_u8() + ('A' as u8);
        out.push(char_val as char)
    }
    out
}

pub fn advance_slice(slice: &mut [AZChar]) -> bool {
    let mut idx = 0;
    let mut success = false;
    while idx < slice.len() {
        if slice[idx].as_u8() == 25 {
            slice[idx].set_u8(0);
            idx += 1;
        }else{
            slice[idx] += AZChar::new(1);
            success = true;
            break;
        }
    }
    success
}

/*
 * Cypher Key Trait,
 * Clone = can be cloned
 * Iterator, iterate through search space
 */
pub trait CypherKey : Clone + Display + Sync + Send + 'static {
    fn dimensionality() -> Range<u32>;
    fn new_default(size: u32) -> Self;
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self;
    //fn new_iterator<I: Iterator<Item = Self>>() -> I;
    fn mutate<R: Rng>(&mut self,rng: &mut R, strength: u32);
    fn advance(&mut self) -> bool;
    fn assign(&mut self, other: &Self);
}

pub trait CypherOutput: Any + Display + Send + Sync + 'static {
    fn as_any_ref(&self) -> &dyn Any;
}

impl<T: CypherKey + Any + Display + Send + Sync + 'static> CypherOutput for T {
    fn as_any_ref(&self) -> &dyn Any {
        self
    }
}

/**
 * Failure to Encrypt or Decrypt
 **/
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum CypherError {
    KeyError,
    InputError,
}

pub trait Cypher : Send + Sync + 'static {
    type Key : CypherKey;
    type Input: Clone + Sync + Send;
    type Output: Clone + Sync + Send;
    fn encrypt(&self, key : &Self::Key, input : &Self::Input, output: &mut Self::Output) -> Result<(),CypherError>;
    fn decrypt(&self, key : &Self::Key, input : &Self::Input, output: &mut Self::Output) -> Result<(),CypherError>;
}

#[cfg(test)]
pub mod test {
    use super::Cypher;
    use super::AZString;
    pub fn test_encrypt<C : Cypher<Input = AZString, Output = AZString>>
    (cypher: &C, key: &C::Key, input : &str, output : &str) {
        let plain = super::convert_to_az(&input.to_owned());
        let mut encrypt = super::AZString::new();
        let mut result = cypher.encrypt(&key,&plain, &mut encrypt);
        assert_eq!(result.is_ok(),true);
        assert_eq!(super::convert_from_az(&encrypt),output);

        let unknown = super::convert_to_az(&output.to_owned());
        let mut decrypt = super::AZString::new();
        result = cypher.decrypt(&key,&unknown, &mut decrypt);
        assert_eq!(result,Ok(()));
        assert_eq!(super::convert_from_az(&decrypt),input);
    }
}

mod solve;
pub use self::solve::{solve_standard_az_bidirectional,solve_standard_az};