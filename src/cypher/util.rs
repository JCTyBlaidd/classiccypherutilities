use std::ops::*;
use std::mem;
use std::fmt::Display;
use std::f64;
use rand::Rng;
use smallvec::SmallVec;
use super::{AZString,CypherOutput,Cypher,convert_from_az};
use std::any::Any;

#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Default, Debug, Hash)]
pub struct AZChar(u8);

impl AZChar {
    #[inline]
    pub fn new(x : u8) -> AZChar {
        AZChar(x % 26)
    }
    #[inline]
    pub fn from_size(s: usize) -> AZChar {
        AZChar((s % 26) as u8)
    }
    #[inline]
    pub fn set_u8(&mut self, x: u8) {
        self.0 = x;
    }
    #[inline]
    pub fn as_u8(&self) -> u8 {
        self.0
    }
    #[inline]
    pub fn as_size(&self) -> usize {
        self.0 as usize
    }
    pub fn mul_inverse(&self) -> Option<AZChar> {
        static INVERSE_TABLE: [Option<AZChar>; 26] = [
            None, Some(AZChar(1 )), //1  * 1  = 1   = 1 (mod 26)
            None, Some(AZChar(9 )), //3  * 9  = 27  = 1 (mod 26)
            None, Some(AZChar(21)), //5  * 21 = 105 = 1 (mod 26)
            None, Some(AZChar(15)), //7  * 15 = 105 = 1 (mod 26)
            None, Some(AZChar(3 )), //9  * 3  = 27  = 1 (mod 26)
            None, Some(AZChar(19)), //11 * 19 = 209 = 1 (mod 26)
            None, None,
            None, Some(AZChar(7 )), //15 * 7  = 105 = 1 (mod 26)
            None, Some(AZChar(23)), //17 * 23 = 391 = 1 (mod 26)
            None, Some(AZChar(11)), //19 * 11 = 209 = 1 (mod 26)
            None, Some(AZChar(5 )), //21 * 5  = 105 = 1 (mod 26)
            None, Some(AZChar(17)), //23 * 17 = 391 = 1 (mod 26)
            None, Some(AZChar(25)), //25 * 25 = 625 = 1 (mod 26)
        ];
        INVERSE_TABLE[self.0 as usize]
    }
    #[inline]
    pub fn sample_26<'s,'a,T>(&'s self,sample: &'a [T;26]) -> &'a T {
        unsafe {
            sample.get_unchecked(self.as_size())
        }
    }
}

impl Add for AZChar {
    type Output = AZChar;
    #[inline]
    fn add(self, other: AZChar) -> AZChar {
        AZChar(self.0.wrapping_add(other.0) % 26)
    }
}

impl AddAssign for AZChar {
    #[inline]
    fn add_assign(&mut self, other: AZChar) {
        *self = *self + other;
    }
}

impl Neg for AZChar {
    type Output = AZChar;
    #[inline]
    fn neg(self) -> AZChar {
        AZChar(26u8.wrapping_sub(self.0))
    }
}

impl Sub for AZChar {
    type Output = AZChar;
    #[inline]
    fn sub(self, other: AZChar) -> AZChar {
        let v = 26u8.wrapping_sub(other.0);
        AZChar(self.0.wrapping_add(v) % 26)
    }
}

impl SubAssign for AZChar {
    #[inline]
    fn sub_assign(&mut self, other: AZChar) {
        *self = *self - other;
    }
}

impl Mul for AZChar {
    type Output = AZChar;
    #[inline]
    fn mul(self, other: AZChar) -> AZChar {
        let tmp: u32 = (self.0 as u32).wrapping_mul(other.0 as u32);
        AZChar((tmp % 26) as u8)
    }
}

impl MulAssign for AZChar {
    #[inline]
    fn mul_assign(&mut self, other: AZChar) {
        *self = *self * other;
    }
}

impl Display for AZChar {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f,"{}",(self.0 + ('A') as u8) as char)
    }
}

#[derive(Clone,Default,Debug)]
pub struct OrderedKey {
    pub order : SmallVec<[usize;8]>,
}

impl OrderedKey {
    pub fn from_size(size: usize) -> Self{
        let mut order = SmallVec::new();
        order.resize(size,Default::default());
        for (i,v) in order.iter_mut().enumerate() {
            *v = i
        }
        OrderedKey {
            order
        }
    }
    pub fn from_rng<R: Rng>(size: usize, rng: &mut R) -> Self {
        let mut default = Self::from_size(size);
        for _i in 0..size {
            default.mutate(rng,2);
        }
        default
    }
    #[inline]
    pub fn mutate<R: Rng>(&mut self,rng: &mut R, strength: u32) {
        for _i in 0..strength {
            let idx1 = rng.gen_range(0,self.order.len());
            let idx2 = rng.gen_range(0,self.order.len());
            self.order.swap(idx1,idx2);
        }
    }
    pub fn advance(&mut self) -> bool {

        //Find the index of the last increase to the value
        let increase = self.order.windows(2).enumerate().rev().find_map(|(idx,pair)| {
            if pair[1] > pair[0] {
                Some(idx)
            }else{
                None
            }
        });
        match increase {
            Some(increase_index) => {

                //increase idx to next largest to the right,
                // with remainder on the right sorted from reverse sorted
                let afterwards : &mut[usize] = &mut self.order[increase_index..];
                let (start,after) = afterwards.split_first_mut().unwrap();

                after.reverse();
                let next_largest = after.iter_mut()
                    .filter(|x| {
                        **x > *start
                    }).next().unwrap();
                mem::swap(start, next_largest);

                //Continue
                true
            }
            None => {

                //Finished
                false
            }
        }
    }

    #[inline]
    pub fn assign(&mut self, other: &OrderedKey) {
        self.order.copy_from_slice(&other.order);
    }
}


pub struct ResultContainer {
    result: Vec<(Box<dyn CypherOutput>,Option<AZString>,f64)>,
    size: usize,
}

struct ModifiedContainer {
    prefix: String,
    value: Box<dyn CypherOutput>,
    postfix: String
}

impl CypherOutput for ModifiedContainer {
    fn as_any_ref(&self) -> &dyn Any {
        self
    }
}

impl Display for ModifiedContainer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f,"{}{}{}",self.prefix,self.value,self.postfix)
    }
}

impl ResultContainer {
    pub fn new(size: usize) -> ResultContainer {
        ResultContainer {
            result: Vec::new(),
            size,
        }
    }
    pub fn add(&mut self, score: f64,key: Box<dyn CypherOutput>,res: Option<AZString>) {
        let minimum = self.result.first()
            .map(|(_,_,s)| s);
        match minimum {
            Some(val) => if score > *val || self.result.len() < self.size {
                self.result.push( (key,res,score));
                self.result.sort_unstable_by(|(_,_,a),(_,_,b)| {
                    a.partial_cmp(b).unwrap()
                });
                while self.result.len() > self.size {
                    self.result.remove(0);
                }
            },
            None => {
                self.result.push((key,res,score));
            }
        }
    }

    pub fn process_all_outputs<C: Cypher<Output = AZString>>(&mut self, cypher: &C,input: &C::Input)
        where C::Key : CypherOutput
    {
        for (d,i,_) in self.result.iter_mut() {
            if i.is_none() {
                let key : &C::Key = d.as_any_ref().downcast_ref::<C::Key>().unwrap();
                let mut output = AZString::new();
                if let Ok(()) = cypher.decrypt(key,input,&mut output) {
                    *i = Some(output);
                }
            }
        }
    }

    pub fn join(&mut self, other: ResultContainer) {
        for (d,i,s) in other.result.into_iter() {
            if self.result.iter().find(|v| {v.1.eq(&i)}).is_none() {
                self.add(s, d, i);
            }
        }
    }

    pub fn best_score(&self) -> f64 {
        match self.result.last() {
            Some(val) => {
                val.2
            },
            None => {
                f64::NEG_INFINITY
            }
        }
    }

    pub fn worst_score(&self) -> f64 {
        match self.result.first() {
            Some(val) => {
                val.2
            },
            None => {
                f64::NEG_INFINITY
            }
        }
    }

    pub fn will_append(&self, score: f64) -> bool {
        score > self.worst_score() || self.result.len() < self.size
    }

    pub fn as_slice(&self) -> &[(Box<dyn CypherOutput>,Option<AZString>,f64)] {
        self.result.as_slice()
    }

    pub fn get_best_result(&self) -> AZString {
        match self.result.last() {
            Some(val) => {
                val.1.clone().expect("Best Result hasn't had result attached!")
            },
            None => {
                panic!("No Result Found")
            }
        }
    }

    pub fn get_best_key(&self) -> String {
        match self.result.last() {
            Some(val) => {
                val.0.to_string()
            },
            None => {
                panic!("No Result Found")
            }
        }
    }

    pub fn append_to_display(&mut self, prefix: String, postfix: String) {
        let mut other_result = Vec::with_capacity(self.result.len());
        mem::swap(&mut self.result, &mut other_result);
        other_result.into_iter().map(|(d,i,s)| {
            let modified = ModifiedContainer {
                prefix: prefix.clone(),
                value: d,
                postfix: postfix.clone(),
            };
            let boxed : Box<dyn CypherOutput> = Box::new(modified);
            (boxed,i,s)
        }).for_each(|e| {
            self.result.push(e);
        })
    }
}

impl Display for ResultContainer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        writeln!(f,"Results: ")?;
        for (key,result,score) in self.result.iter().rev() {
            writeln!(f, " {}: {}", score, key)?;
            if let Some(az_str) = result {
                writeln!(f, "    {}", convert_from_az(az_str))?;
            }else{
                panic!("Result Tuple is None");
            }
        }
        writeln!(f)
    }
}



#[cfg(test)]
mod test {
    use super::AZChar;
    #[test]
    fn test_char() {
        let a = AZChar(4);
        let b = AZChar(15);
        let c = AZChar(24);
        let d = b + c;
        let e = a + b + c;
        let f = a * b * c;
        let g = a - b;
        let h = a - c;
        assert_eq!(d,AZChar(13));
        assert_eq!(e,AZChar(17));
        assert_eq!(f,AZChar(10));
        assert_eq!(g,AZChar(15));
        assert_eq!(h,AZChar(06));

        assert_eq!(AZChar::new(27),AZChar::new(1));
        for i in 1..25 {
            let az = AZChar::new(i);
            let inv = az.mul_inverse();
            if let Some(inverse) = inv {
                let mul = az * inverse;
                assert_eq!(mul.0,1);
            }
        }
    }

    use super::OrderedKey;
    fn adv_order(order: &mut OrderedKey,target: &[usize],adv: bool) {
        assert_eq!(&order.order.as_slice(),&target);
        assert_eq!(order.advance(),adv);
    }
    #[test]
    fn test_order() {
        let mut order = OrderedKey::from_size(5);
        adv_order(&mut order,&[0, 1, 2, 3, 4], true);
        adv_order(&mut order,&[0, 1, 2, 4, 3], true);
        adv_order(&mut order,&[0, 1, 3, 2, 4], true);
        adv_order(&mut order,&[0, 1, 3, 4, 2], true);
        adv_order(&mut order,&[0, 1, 4, 2, 3], true);
        adv_order(&mut order,&[0, 1, 4, 3, 2], true);
        adv_order(&mut order,&[0, 2, 1, 3, 4], true);
        adv_order(&mut order,&[0, 2, 1, 4, 3], true);
        adv_order(&mut order,&[0, 2, 3, 1, 4], true);
        adv_order(&mut order,&[0, 2, 3, 4, 1], true);
        adv_order(&mut order,&[0, 2, 4, 1, 3], true);
        adv_order(&mut order,&[0, 2, 4, 3, 1], true);
        adv_order(&mut order,&[0, 3, 1, 2, 4], true);
        adv_order(&mut order,&[0, 3, 1, 4, 2], true);
        adv_order(&mut order,&[0, 3, 2, 1, 4], true);
        adv_order(&mut order,&[0, 3, 2, 4, 1], true);
        adv_order(&mut order,&[0, 3, 4, 1, 2], true);
        adv_order(&mut order,&[0, 3, 4, 2, 1], true);
        adv_order(&mut order,&[0, 4, 1, 2, 3], true);
        adv_order(&mut order,&[0, 4, 1, 3, 2], true);
        adv_order(&mut order,&[0, 4, 2, 1, 3], true);
        adv_order(&mut order,&[0, 4, 2, 3, 1], true);
        adv_order(&mut order,&[0, 4, 3, 1, 2], true);
        adv_order(&mut order,&[0, 4, 3, 2, 1], true);
        adv_order(&mut order,&[1, 0, 2, 3, 4], true);
        adv_order(&mut order,&[1, 0, 2, 4, 3], true);
        adv_order(&mut order,&[1, 0, 3, 2, 4], true);
        adv_order(&mut order,&[1, 0, 3, 4, 2], true);
        adv_order(&mut order,&[1, 0, 4, 2, 3], true);
        adv_order(&mut order,&[1, 0, 4, 3, 2], true);
        adv_order(&mut order,&[1, 2, 0, 3, 4], true);
        adv_order(&mut order,&[1, 2, 0, 4, 3], true);
        adv_order(&mut order,&[1, 2, 3, 0, 4], true);
        adv_order(&mut order,&[1, 2, 3, 4, 0], true);
        adv_order(&mut order,&[1, 2, 4, 0, 3], true);
        adv_order(&mut order,&[1, 2, 4, 3, 0], true);
        adv_order(&mut order,&[1, 3, 0, 2, 4], true);
        adv_order(&mut order,&[1, 3, 0, 4, 2], true);
        adv_order(&mut order,&[1, 3, 2, 0, 4], true);
        adv_order(&mut order,&[1, 3, 2, 4, 0], true);
        adv_order(&mut order,&[1, 3, 4, 0, 2], true);
        adv_order(&mut order,&[1, 3, 4, 2, 0], true);
        adv_order(&mut order,&[1, 4, 0, 2, 3], true);
        adv_order(&mut order,&[1, 4, 0, 3, 2], true);
        adv_order(&mut order,&[1, 4, 2, 0, 3], true);
        adv_order(&mut order,&[1, 4, 2, 3, 0], true);
        adv_order(&mut order,&[1, 4, 3, 0, 2], true);
        adv_order(&mut order,&[1, 4, 3, 2, 0], true);
        adv_order(&mut order,&[2, 0, 1, 3, 4], true);
        adv_order(&mut order,&[2, 0, 1, 4, 3], true);
        adv_order(&mut order,&[2, 0, 3, 1, 4], true);
        adv_order(&mut order,&[2, 0, 3, 4, 1], true);
        adv_order(&mut order,&[2, 0, 4, 1, 3], true);
        adv_order(&mut order,&[2, 0, 4, 3, 1], true);
        adv_order(&mut order,&[2, 1, 0, 3, 4], true);
        adv_order(&mut order,&[2, 1, 0, 4, 3], true);
        adv_order(&mut order,&[2, 1, 3, 0, 4], true);
        adv_order(&mut order,&[2, 1, 3, 4, 0], true);
        adv_order(&mut order,&[2, 1, 4, 0, 3], true);
        adv_order(&mut order,&[2, 1, 4, 3, 0], true);
        adv_order(&mut order,&[2, 3, 0, 1, 4], true);
        adv_order(&mut order,&[2, 3, 0, 4, 1], true);
        adv_order(&mut order,&[2, 3, 1, 0, 4], true);
        adv_order(&mut order,&[2, 3, 1, 4, 0], true);
        adv_order(&mut order,&[2, 3, 4, 0, 1], true);
        adv_order(&mut order,&[2, 3, 4, 1, 0], true);
        adv_order(&mut order,&[2, 4, 0, 1, 3], true);
        adv_order(&mut order,&[2, 4, 0, 3, 1], true);
        adv_order(&mut order,&[2, 4, 1, 0, 3], true);
        adv_order(&mut order,&[2, 4, 1, 3, 0], true);
        adv_order(&mut order,&[2, 4, 3, 0, 1], true);
        adv_order(&mut order,&[2, 4, 3, 1, 0], true);
        adv_order(&mut order,&[3, 0, 1, 2, 4], true);
        adv_order(&mut order,&[3, 0, 1, 4, 2], true);
        adv_order(&mut order,&[3, 0, 2, 1, 4], true);
        adv_order(&mut order,&[3, 0, 2, 4, 1], true);
        adv_order(&mut order,&[3, 0, 4, 1, 2], true);
        adv_order(&mut order,&[3, 0, 4, 2, 1], true);
        adv_order(&mut order,&[3, 1, 0, 2, 4], true);
        adv_order(&mut order,&[3, 1, 0, 4, 2], true);
        adv_order(&mut order,&[3, 1, 2, 0, 4], true);
        adv_order(&mut order,&[3, 1, 2, 4, 0], true);
        adv_order(&mut order,&[3, 1, 4, 0, 2], true);
        adv_order(&mut order,&[3, 1, 4, 2, 0], true);
        adv_order(&mut order,&[3, 2, 0, 1, 4], true);
        adv_order(&mut order,&[3, 2, 0, 4, 1], true);
        adv_order(&mut order,&[3, 2, 1, 0, 4], true);
        adv_order(&mut order,&[3, 2, 1, 4, 0], true);
        adv_order(&mut order,&[3, 2, 4, 0, 1], true);
        adv_order(&mut order,&[3, 2, 4, 1, 0], true);
        adv_order(&mut order,&[3, 4, 0, 1, 2], true);
        adv_order(&mut order,&[3, 4, 0, 2, 1], true);
        adv_order(&mut order,&[3, 4, 1, 0, 2], true);
        adv_order(&mut order,&[3, 4, 1, 2, 0], true);
        adv_order(&mut order,&[3, 4, 2, 0, 1], true);
        adv_order(&mut order,&[3, 4, 2, 1, 0], true);
        adv_order(&mut order,&[4, 0, 1, 2, 3], true);
        adv_order(&mut order,&[4, 0, 1, 3, 2], true);
        adv_order(&mut order,&[4, 0, 2, 1, 3], true);
        adv_order(&mut order,&[4, 0, 2, 3, 1], true);
        adv_order(&mut order,&[4, 0, 3, 1, 2], true);
        adv_order(&mut order,&[4, 0, 3, 2, 1], true);
        adv_order(&mut order,&[4, 1, 0, 2, 3], true);
        adv_order(&mut order,&[4, 1, 0, 3, 2], true);
        adv_order(&mut order,&[4, 1, 2, 0, 3], true);
        adv_order(&mut order,&[4, 1, 2, 3, 0], true);
        adv_order(&mut order,&[4, 1, 3, 0, 2], true);
        adv_order(&mut order,&[4, 1, 3, 2, 0], true);
        adv_order(&mut order,&[4, 2, 0, 1, 3], true);
        adv_order(&mut order,&[4, 2, 0, 3, 1], true);
        adv_order(&mut order,&[4, 2, 1, 0, 3], true);
        adv_order(&mut order,&[4, 2, 1, 3, 0], true);
        adv_order(&mut order,&[4, 2, 3, 0, 1], true);
        adv_order(&mut order,&[4, 2, 3, 1, 0], true);
        adv_order(&mut order,&[4, 3, 0, 1, 2], true);
        adv_order(&mut order,&[4, 3, 0, 2, 1], true);
        adv_order(&mut order,&[4, 3, 1, 0, 2], true);
        adv_order(&mut order,&[4, 3, 1, 2, 0], true);
        adv_order(&mut order,&[4, 3, 2, 0, 1], true);
        adv_order(&mut order,&[4, 3, 2, 1, 0], false);

        order = OrderedKey::from_size(4);
        adv_order(&mut order,&[0,1,2,3],true);
        adv_order(&mut order,&[0,1,3,2],true);
        adv_order(&mut order,&[0,2,1,3],true);
        adv_order(&mut order,&[0,2,3,1],true);
        adv_order(&mut order,&[0,3,1,2],true);
        adv_order(&mut order,&[0,3,2,1],true);
        adv_order(&mut order,&[1,0,2,3],true);
        adv_order(&mut order,&[1,0,3,2],true);
        adv_order(&mut order,&[1,2,0,3],true);
        adv_order(&mut order,&[1,2,3,0],true);
        adv_order(&mut order,&[1,3,0,2],true);
        adv_order(&mut order,&[1,3,2,0],true);
        adv_order(&mut order,&[2,0,1,3],true);
        adv_order(&mut order,&[2,0,3,1],true);
        adv_order(&mut order,&[2,1,0,3],true);
        adv_order(&mut order,&[2,1,3,0],true);
        adv_order(&mut order,&[2,3,0,1],true);
        adv_order(&mut order,&[2,3,1,0],true);
        adv_order(&mut order,&[3,0,1,2],true);
        adv_order(&mut order,&[3,0,2,1],true);
        adv_order(&mut order,&[3,1,0,2],true);
        adv_order(&mut order,&[3,1,2,0],true);
        adv_order(&mut order,&[3,2,0,1],true);
        adv_order(&mut order,&[3,2,1,0],false);

        order = OrderedKey::from_size(3);
        adv_order(&mut order,&[0,1,2],true);
        adv_order(&mut order,&[0,2,1],true);
        adv_order(&mut order,&[1,0,2],true);
        adv_order(&mut order,&[1,2,0],true);
        adv_order(&mut order,&[2,0,1],true);
        adv_order(&mut order,&[2,1,0],false);

        order = OrderedKey::from_size(2);
        adv_order(&mut order,&[0,1],true);
        adv_order(&mut order,&[1,0],false);

        order = OrderedKey::from_size(1);
        adv_order(&mut order,&[0],false);

        order = OrderedKey::from_size(0);
        adv_order(&mut order, &[], false);
    }
}
