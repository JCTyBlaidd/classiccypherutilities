use super::{ResultContainer,DynamicOp,DynamicOpList};
use rayon;

//Dispatch Tree: {2}
// No Change
// Reverse Input
pub fn start_dynamic_tree_solve(input: Vec<u32>,tokens: usize, results: &mut ResultContainer) {
    let reversed = input
        .iter()
        .map(|c|*c)
        .rev()
        .collect::<Vec<_>>();
    let mut results_normal = None;
    let mut results_reverse = None;
    rayon::scope(|scope| {
        scope.spawn(|_| {
            let res = dynamic_tree_remove(input,tokens);
            results_normal = Some(res);
        });
        scope.spawn(|_| {
            let res = dynamic_tree_remove(reversed,tokens);
            results_reverse = Some(res);
        });
    });
    if let Some(res) = results_normal {
        results.join(res);
    }
    if let Some(res) = results_normal {
        results.join(res);
    }
}

//Dispatch Tree: {tokens + 1}
// No Change
// Remove 1 Token
pub fn dynamic_tree_remove(input: Vec<u32>, tokens: usize) -> ResultContainer {
    rayon::scope(|scope| {

    });
    ResultContainer::new(4)
}

//Dispatch Tree: {tokens + 7}
// Split By Length
// Split By Value
pub fn dynamic_tree_split() {

}
//Dispatch Tree {2}:
// No Change
// Transpose Inputs
pub fn dynamic_tree_transpose() {

}

//Dispatch Tree:
// Convert Rows To Unique Values
pub fn dynamic_tree_row_convert() {

}