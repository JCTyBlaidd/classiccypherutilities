use std::fmt::{Formatter,Error,Display};
use std::collections::HashMap;
use self::DynamicOp::*;
use super::{ResultContainer,AZString,convert_from_az};

mod solve;

//Dynamic Decryption Search
#[derive(Clone, Debug)]
pub enum DynamicOp {
    Reverse,
    RemoveValue(u32),
    SplitByValue(u32),
    Transpose(u32),
    ConvertRowToValue,
    ShuffleBy(Vec<u32>),
    PackToAZ,
    ChiShift(u8),
    Substitution(AZString),
}

impl Display for DynamicOp {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        match self {
            Reverse => {
                write!(f,"Reverse")
            },
            RemoveValue(k) => {
                write!(f,"Remove( {} )",k)
            },
            SplitByValue(k) => {
                write!(f,"Split( {} )",k)
            },
            Transpose(k) => {
                write!(f,"Transpose( {} )",k)
            },
            ConvertRowToValue => {
                write!(f,"RowToValue")
            },
            ShuffleBy(vec) => {
                write!(f,"ShuffleBy(");
                if let Some((first,rest)) =  vec.split_first() {
                    write!(f,"{}",first);
                    for value in rest.iter() {
                        write!(f,",{}",value);
                    }
                }
                write!(f,")")
            },
            PackToAZ => {
                write!(f,"ToAZ")
            },
            ChiShift(shift) => {
                write!(f,"ChiShift( {} )",shift)
            }
            Substitution(values) => {
                write!(f,"SolveSubstitution( \"{}\" )",convert_from_az(&values))
            }
        }
    }
}


#[derive(Clone, Debug)]
pub struct DynamicOpList {
    ops: Vec<DynamicOp>,
}

impl Display for DynamicOpList {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f,"Dynamic[ ");
        for op in self.ops.iter() {
            write!(f,"{} ",op);
        }
        write!(f,"]")
    }
}


pub fn solve_dynamic(input: &String) -> ResultContainer {
    let mut results = ResultContainer::new(10);
    let mut uniques = input.chars().collect::<Vec<_>>();
    uniques.sort_unstable();
    uniques.dedup();
    let map = uniques
        .into_iter()
        .enumerate()
        .map(|(a,b)|(b,a as u32))
        .collect::<HashMap<_,_>>();
    let input = input
        .chars()
        .map(|chr| *map.get(&chr).unwrap())
        .collect::<Vec<_>>();
    solve::start_dynamic_tree_solve(input,map.len(),&mut results);
    results
}

