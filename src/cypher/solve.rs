use rand::{XorShiftRng,FromEntropy};
use super::*;

pub fn solve_standard_az_bidirectional(input: &AZString, lang: &LanguageMetadata, print: bool) -> ResultContainer {
    let mut result_normal = None;
    let mut result_reverse = None;
    let mut rev_input = input.clone();
    rev_input.reverse();
    rayon::scope(|scope| {
        scope.spawn(|_| {
            let res = solve_standard_az(&input, &lang, print);
            result_normal = Some(res);
        });
        scope.spawn(|_| {
            let res = solve_standard_az(&rev_input, &lang, print);
            result_reverse = Some(res);
        });
    });
    let mut result = result_normal.unwrap();
    let mut result_reverse = result_reverse.unwrap();
    result_reverse.append_to_display("Reversed{ ".to_string(), " }".to_string());
    result.join(result_reverse);
    result
}

pub fn solve_standard_az(input: &AZString, lang: &LanguageMetadata, print: bool) -> ResultContainer {
    let mut affine_exh = None;
    let mut vignere_exh = None;
    let mut vignere_hcl = None;
    let mut autokey_exh = None;
    let mut autokey_hcl = None;
    let mut transpose_exh = None;
    let mut transpose_hcl = None;
    let mut column_exh = None;
    let mut column_hcl = None;
    let mut keyword_exh = None;
    let mut keyword_sml = None;
    let mut hill2x2_exh = None;
    let mut hill3x3_sml = None;

    rayon::scope(|scope| {
        scope.spawn(|_| {
            let cypher = affine::AffineCypher;
            let res = strategy::exhaustive_search(&cypher, &input, &lang, 2, 5);
            affine_exh = Some(res);
            if print {
                println!("Finished: Affine Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = vignere::VignereCypher;
            let res = strategy::exhaustive_search_range(&cypher, &input, &lang, 1..4, 5);
            vignere_exh = Some(res);
            if print {
                println!("Finished: Vignere Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = vignere::VignereCypher;
            let res = strategy::async_result(4..15, |i| {
                let mut rng = XorShiftRng::from_entropy();
                strategy::hill_climb(
                    &cypher,
                    &input,
                    &lang,
                    i,
                    3,
                    (i * 1000) as usize,
                    1,
                    4
                    , &mut rng
                )
            }, 10);
            vignere_hcl = Some(res);
            if print {
                println!("Finished: Vignere Hill-Climbing");
            }
        });
        scope.spawn(|_| {
            let cypher = vignere::AutoKeyCypher;
            let res = strategy::exhaustive_search_range(&cypher, &input, &lang, 1..4, 5);
            autokey_exh = Some(res);
            if print {
                println!("Finished: Vignere Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = vignere::AutoKeyCypher;
            let res = strategy::async_result(4..15, |i| {
                let mut rng = XorShiftRng::from_entropy();
                strategy::hill_climb(
                    &cypher,
                    &input,
                    &lang,
                    i,
                    3,
                    (i * 1000) as usize,
                    1,
                    4
                    , &mut rng
                )
            }, 10);
            autokey_hcl = Some(res);
            if print {
                println!("Finished: Vignere Hill-Climbing");
            }
        });
        scope.spawn(|_| {
            let cypher = transposition::TransposeCypher;
            let res = strategy::exhaustive_search_range(&cypher, &input, &lang, 1..6, 5);
            transpose_exh = Some(res);
            if print {
                println!("Finished: Transposition Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = transposition::TransposeCypher;
            let res = strategy::async_result(6..15, |i| {
                let mut rng = XorShiftRng::from_entropy();
                let mutate = if i < 8 { 2 } else { 3 };
                strategy::hill_climb(
                    &cypher,
                    &input,
                    &lang,
                    i,
                    3,
                    (i * i * 1000 + 25000) as usize,
                    1,
                    mutate,
                    &mut rng
                )
            }, 10);
            transpose_hcl = Some(res);
            if print {
                println!("Finished: Transposition Hill-Climbing");
            }
        });
        scope.spawn(|_| {
            let cypher = transposition::ColumnarCypher;
            let res = strategy::exhaustive_search_range(&cypher, &input, &lang, 1..6, 5);
            column_exh = Some(res);
            if print {
                println!("Finished: Columnar Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = transposition::ColumnarCypher;
            let res = strategy::async_result(6..12, |i| {
                let mut rng = XorShiftRng::from_entropy();
                let mutate = if i < 8 { 2 } else { 3 };
                strategy::hill_climb(
                    &cypher,
                    &input,
                    &lang,
                    i,
                    3,
                    (i * i * 1000 + 25000) as usize,
                    1,
                    mutate,
                    &mut rng
                )
            }, 10);
            column_hcl = Some(res);
            if print {
                println!("Finished: Columnar Hill-Climbing");
            }
        });
        scope.spawn(|_| {
            let cypher = keyword::KeywordCypher;
            let res = strategy::exhaustive_search_range(&cypher, &input, &lang, 1..5, 5);
            keyword_exh = Some(res);
            if print {
                println!("Finished: Keyword Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = keyword::KeywordCypher;
            let res = strategy::async_result(5..12, |i| {
                let mut rng = XorShiftRng::from_entropy();
                strategy::simulated_annealing(
                    &cypher,
                    &input,
                    &lang,
                    i,
                    2,
                    (i * 2000 + 10000) as usize,
                    2,
                    &mut rng
                )
            }, 10);
            keyword_sml = Some(res);
            if print {
                println!("Finished: Keyword Simulated-Annealing");
            }
        });
        scope.spawn(|_| {
            let cypher = hill::Hill2x2Cypher;
            let res = strategy::exhaustive_search(&cypher, &input, &lang, 2, 3);
            hill2x2_exh = Some(res);
            if print {
                println!("Finished: Hill2x2 Exhaustive");
            }
        });
        scope.spawn(|_| {
            let cypher = hill::Hill3x3Cypher;
            let mut rng = XorShiftRng::from_entropy();
            let res = strategy::simulated_annealing(
                &cypher,
                &input,
                &lang,
                3,
                5,
                5000,
                6,
                &mut rng
            );
            hill3x3_sml = Some(res);
            if print {
                println!("Finished: Hill3x3 Simulated-Annealing");
            }
        });
    });

    let mut total = ResultContainer::new(10);
    if let Some(res) = affine_exh {
        total.join(res);
    }
    if let Some(res) = vignere_exh {
        total.join(res);
    }
    if let Some(res) = vignere_hcl {
        total.join(res);
    }
    if let Some(res) = autokey_exh {
        total.join(res);
    }
    if let Some(res) = autokey_hcl {
        total.join(res);
    }
    if let Some(res) = transpose_exh {
        total.join(res);
    }
    if let Some(res) = transpose_hcl {
        total.join(res);
    }
    if let Some(res) = column_exh {
        total.join(res);
    }
    if let Some(res) = column_hcl {
        total.join(res);
    }
    if let Some(res) = keyword_exh {
        total.join(res);
    }
    if let Some(res) = keyword_sml {
        total.join(res);
    }
    if let Some(res) = hill2x2_exh {
        total.join(res);
    }
    if let Some(res) = hill3x3_sml {
        total.join(res);
    }
    total
}
