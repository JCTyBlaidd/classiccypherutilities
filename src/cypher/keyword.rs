use super::{AZChar,AZString,CypherError,Cypher,CypherKey,Rng,Range,convert_from_az,advance_slice};
use std::fmt::Display;
use std::fmt;
use smallvec::*;

#[derive(Clone, Default, Debug)]
pub struct KeywordKey {
    key : SmallVec<[AZChar;32]>,
}

impl Display for KeywordKey {
    //TODO: TEST FOR COMPLETE KEYWORD SECTION
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let slice : &[AZChar] = &self.key[..];
        let key  = slice.iter().enumerate()
            .filter_map(|(idx,val)| {
            if slice[..idx].contains(val) {
                None
            }else{
                Some(*val)
            }
        }).collect::<Vec<_>>();
        write!(f,"Keyword( \"{}\" )",convert_from_az(&key))
    }
}

impl CypherKey for KeywordKey {
    fn dimensionality() -> Range<u32> {
        1..26
    }
    fn new_default(size: u32) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize,Default::default());
        KeywordKey {
            key,
        }
    }
    fn new_random<R: Rng>(size: u32, rng: &mut R) -> Self {
        let mut key = SmallVec::new();
        key.resize(size as usize, Default::default());
        for k in key.iter_mut() {
            *k = AZChar::new(rng.gen_range(0,26))
        }
        KeywordKey {
            key,
        }
    }
    fn mutate<R: Rng>(&mut self,rng: &mut R, strength: u32) {
        for _ in 0..strength {
            let idx = rng.gen_range(0,self.key.len());
            self.key[idx].set_u8(rng.gen_range(0,26) as u8);
        }
    }
    fn advance(&mut self) -> bool {
        advance_slice(self.key.as_mut_slice())
    }
    fn assign(&mut self,other: &Self) {
        self.key.copy_from_slice(&other.key);
    }
}

pub struct KeywordCypher;

fn make_key_substitution(key: &[AZChar], substitution : &mut [AZChar;26]) {
    let mut write_index = 0;
    for (i,k) in key.iter().enumerate() {
        let prefix = &key[..i];
        if !prefix.contains(k) {
            substitution[write_index] = k.clone();
            write_index += 1;
        }
    }
    for i in write_index..26 {
        let mut prev = substitution[write_index - 1];
        loop {
            prev += AZChar::new(1);
            let search = &substitution[..i];
            if !search.contains(&prev) {
                break;
            }
        }
        substitution[i] = prev;
    }
}

impl Cypher for KeywordCypher {
    type Key = KeywordKey;
    type Input = AZString;
    type Output = AZString;
    fn encrypt(&self, key : &KeywordKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mut substitution = [AZChar::default();26];
        make_key_substitution(&key.key.as_slice(), &mut substitution);
        for (i,o) in input.iter().zip(output.iter_mut()) {
            *o = *i.sample_26(&substitution);
        }
        Ok(())
    }
    fn decrypt(&self, key : &KeywordKey, input : &AZString, output: &mut AZString) -> Result<(),CypherError> {
        output.resize(input.len(),Default::default());
        let mut substitution = [AZChar::default();26];
        make_key_substitution(&key.key.as_slice(),&mut substitution);
        let mut reversed = [AZChar::default();26];
        for i in 0..26 {
            let map = substitution[i];
            let val = AZChar::new(i as u8);
            reversed[map.as_u8() as usize] = val;
        }
        for (i,o) in input.iter().zip(output.iter_mut()) {
            *o = *i.sample_26(&reversed);
        }
        Ok(())
    }
}


#[cfg(test)]
mod test {
    use super::{KeywordCypher,KeywordKey};
    use super::super::test;
    #[test]
    fn test_encrypt() {
        let cypher : KeywordCypher = KeywordCypher;
        let key1 = KeywordKey {
            key: super::super::convert_to_az(&"KRYPTOSA".to_string()).into_iter().collect(),
        };
        test::test_encrypt(
            &cypher,
            &key1,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "KRYPTOSABCDEFGHIJLMNQUVWXZ"
        );
        let key2 = KeywordKey {
            key: super::super::convert_to_az(&"KRYPTOS".to_string()).into_iter().collect(),
        };
        test::test_encrypt(
            &cypher,
            &key2,
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            "KRYPTOSUVWXZABCDEFGHIJLMNQ"
        );
    }
}