#![feature(nll)]

#[macro_use]
extern crate smallvec;
extern crate rand;
extern crate rayon;

mod cypher;
mod language;

pub use language::{LanguageMetadata,get_english};
pub use cypher::{solve_standard_az_bidirectional,AZString,convert_to_az,convert_from_az};

fn default_solve(input: &str) -> (String,String) {
    let az_input = convert_to_az(input);
    let english = get_english();
    let result = solve_standard_az_bidirectional(&az_input,&english,false);
    let best_standard = convert_from_az(&result.get_best_result());

    (best_standard, result.get_best_key())
}

fn main() {
    //let input = "NRAT IMOS ONNG AAIU GNYO ISOE ETAW RDRG NFOI OLOK RALT EEMU OROL ELYT IWLL IONO NRFA TEST EHIL DAEL TMOC IRNO EFES HOMT GNWI IEME TYNU ITTP PORO HTIS EHOA VTIY AMKL WUCO VEEH IBTE LNID CANI MXAA DTYN AEHR ETDE OUNE LARE AHTW WIYN GLAZ PPZL ABUE SIAR EHTE RTYG BUOM SOLO VOEC LITL LSCA TIPE TIMA EMDI MCUH ISIW THGH TINE SOOM TELA DDOC ULTA HTIT ISCS YLAP HCAM TIEA MTHN HPDI ATUO NOSI ETNW THEH OHUG TVEE RNNI EILO DONE OEPL PIHN GTEV RYEI RNGO YEST HRAE TEOE PLPO BUTA ITON AOFR MNCU HIMT OSOG AHVE DOCU LYTW HEOO NWHK ODNT IREN STPE ATHE SETO EIRT SHCE AUIS TMEM DAKN ELDA IEDT AYNI GLZA SMAI DIED VPSR ODTA APDE TSTE THTH GTAR IDGE OORW TEAA SIWH TTNH AETU THBA MRTS LALY EBOE RTHM ASHR OITG GNAL ITLE RIUO RFOT RSSE ESAL LHTF AIEP DWMS EWAB UOLD CICE SNAE GEHL EPTH LALY EACN RENI KWHE TITA OTOL SIET IREB FONT CIOA FTOR ILFA GDEM ANIH GTTI HTER TCNE AELL IGEI TNTH REIG HSET TVHO ARTY EISK TSHE KTTA CATF HEOO SMET VEEN RLLY PACA TUDO CULT TMHA EYSS TAID NGLB FUIO NACE HAEL CRVA EAHK NWEI IDTH NLUL AFTE TYRI KSPN PMBA EHTE T";    let az_input = convert_to_az(&input.to_owned());
    //let result = solve_standard_az(&az_input,&get_english());
    //println!("{}",result);
}


#[cfg(test)]
mod test {
    use std::fs::*;
    use std::io::*;
    use std::iter::*;

    //To Expensive for Debug Mode
    #[cfg(not(debug_assertions))]
    #[test]
    fn test_results() {
        let paths = read_dir("./test").unwrap();
        for path in paths {
            let entry = path.unwrap();
            let file_name = entry.file_name();
            let id = file_name.into_string().unwrap();
            let id = id.replace("_data.txt","");
            let file = File::open(entry.path()).unwrap();
            let mut buf_reader = BufReader::new(file);
            let mut contents = String::new();
            buf_reader.read_to_string(&mut contents);
            let collect = contents.split_whitespace().collect::<Vec<_>>();
            for (idx,chunk) in collect.chunks(2).enumerate() {
                let cipher = chunk[0];
                let plain = chunk[1];
                let (solve,key) = super::default_solve(cipher);
                let num = (idx / 2) + 1;
                let sec = (('A' as u8) + ((idx % 2) as u8)) as char;
                if solve.as_str().eq(plain) {
                    println!("{} - {}{}: SUCCESS = {}",id,num,sec,key);
                }else{
                    println!("{} - {}{}: FAILURE = {}",id,num,sec,key);
                    println!("    {}",solve);
                    println!("    {}",plain);
                }
            }
        }
    }
}
